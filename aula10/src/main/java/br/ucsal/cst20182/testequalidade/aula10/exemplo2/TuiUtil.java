package br.ucsal.cst20182.testequalidade.aula10.exemplo2;

import java.util.Scanner;

public class TuiUtil {

	public Scanner scanner = new Scanner(System.in);

	public String obterNomeCompleto() {
		String nome;
		String sobrenome;
		System.out.println("Informe o nome:");
		nome = scanner.nextLine();
		System.out.println("Informe o sobrenome:");
		sobrenome = scanner.nextLine();
		return nome + " " + sobrenome;
	}

	public void exibirMensagem(String mensagem) {
		System.out.println("Bom dia!");
		System.out.println(mensagem);
	}

}
