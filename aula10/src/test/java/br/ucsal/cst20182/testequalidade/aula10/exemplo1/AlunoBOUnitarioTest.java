package br.ucsal.cst20182.testequalidade.aula10.exemplo1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.cst20182.testequalidade.aula10.exemplo1.Aluno;
import br.ucsal.cst20182.testequalidade.aula10.exemplo1.AlunoBO;
import br.ucsal.cst20182.testequalidade.aula10.exemplo1.AlunoDAO;
import br.ucsal.cst20182.testequalidade.aula10.exemplo1.DateUtil;

public class AlunoBOUnitarioTest {

	private DateUtil dateUtilMock;

	private AlunoDAO alunoDAOMock;

	private AlunoBO alunoBO;

	@Before
	public void setup() {
		dateUtilMock = Mockito.mock(DateUtil.class);
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateUtilMock);
	}

	@Test
	public void testarCalculoIdade() {

		// Definindo os dados de entrada
		Integer matricula = 90;

		// Configurando o ano atual no DateUtilMock
		Mockito.doReturn(2018).when(dateUtilMock).obterAnoAtual();

		// Configurando o alunoDAOMock
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(1998).build();
		Mockito.doReturn(aluno1).when(alunoDAOMock).encontrarPorMatricula(matricula);

		// Definindo a sa�da esperada
		Integer idadeEsperada = 20;

		// Executando o m�todo que est� sendo testado e obtendo o resultado
		// atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Comparando o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);

		// Verificar as chamadas, tanto ao dateUtilMock quanto ao alunoDAOMock,
		// mas n�o seria essencial para verificar o sucesso da execu��o do
		// calcularIdade.
		Mockito.verify(dateUtilMock).obterAnoAtual();
		Mockito.verify(alunoDAOMock).encontrarPorMatricula(matricula);
	}

	@Test
	public void testarAtualizacaoAluno() {
		// Definir os dados de entrada
		Aluno alunoAtivo = AlunoBuilder.umAlunoAtivo().build();

		// Executar o m�todo que est� sendo testado
		alunoBO.atualizar(alunoAtivo);

		// Verificar se o m�todo salvar da inst�ncia alunoDAO foi chamado,
		// passando como par�metro o objeto alunoAtivo.
		Mockito.verify(alunoDAOMock).salvar(alunoAtivo);
	}
}
