package br.ucsal.cst20182.testequalidade.aula10.exemplo2;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

public class TuiUtilUnitarioTestSemMockito {

	@Test
	public void testarObterNomeCompleto() {

		// Definir dados de entrada
		String nome = "Claudio";
		String sobrenome = "Neiva";

		// Setup sem apoio do mockito para fazer o "stub do Scanner"
		ByteArrayInputStream inStub = new ByteArrayInputStream((nome + "\n" + sobrenome).getBytes());
		System.setIn(inStub);
		TuiUtil tuiUtil = new TuiUtil();

		// Definir sa�da esperada
		String nomeCompletoEsperado = "Claudio Neiva";

		// Executar o m�todo a ser testado e obter o resultado atual
		String nomeCompletoAtual = tuiUtil.obterNomeCompleto();

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

}
