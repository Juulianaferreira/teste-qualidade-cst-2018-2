package br.ucsal.cst20182.testequalidade.atividade2;

import org.junit.Assert;
import org.junit.Test;

public class Atividade2Test {

	@Test
	public void testarFatorial0() {
		// Definir os dados de entrada
		Integer n = 0;

		// Definir a sa�da esperada
		Long fatorialEsperado = 1L;

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial5() {
		// Definir os dados de entrada
		Integer n = 5;

		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
