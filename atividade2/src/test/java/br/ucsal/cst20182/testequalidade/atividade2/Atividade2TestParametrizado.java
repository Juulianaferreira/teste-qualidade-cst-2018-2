package br.ucsal.cst20182.testequalidade.atividade2;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Atividade2TestParametrizado {

	// Definir casos de teste
	@Parameters(name="{index} fatorial({0})={1}")
	public static Collection<Object[]> obterDadosTeste() {
		return Arrays.asList(new Object[][] { { 0, 1L }, { 2, 2L }, { 3, 6L }, { 5, 120L } });
	}

	// Definir os dados de entrada
	@Parameter
	public Integer n;
	
	// Definir a sa�da esperada
	@Parameter(1)
	public Long fatorialEsperado;
	
	@Test
	public void testarFatorial() {
		// Executar o m�todo que est� sendo testado e obter o resultado atual
		Long fatorialAtual = Atividade2.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
