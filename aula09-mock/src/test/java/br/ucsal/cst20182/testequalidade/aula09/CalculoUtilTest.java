package br.ucsal.cst20182.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculoUtilTest {

	public CalculoUtil calculoUtil;

	@Before
	public void setup() {
		calculoUtil = new CalculoUtil();
	}

	@Test
	public void testarFatorial4() {
		int n = 4;
		Long fatorialEsperado = 24L;
		Long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
