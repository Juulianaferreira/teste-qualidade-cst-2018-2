package br.ucsal.cst20182.testequalidade.aula09;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

public class AlunoDAOMock extends AlunoDAO {

	// Map<nome-do-m�todo, Map<par�metro, quantidade-chamadas>>
	private Map<String, Map<Object, Integer>> chamadasMap = new HashMap<>();

	@Override
	public void salvar(Aluno aluno) {
		registrarChamada("salvar", aluno);
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		registrarChamada("encontrarPorMatricula", matricula);
		if (matricula.equals(5)) {
			return AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(1998).build();
		}
		return null;
	}

	public void verificarChamada(String nomeMetodo, Object objeto, Integer qtdChamadasEsperada) {
		Integer qtdChamadasAtual = 0;
		if (chamadasMap.containsKey(nomeMetodo)) {
			if (chamadasMap.get(nomeMetodo).containsKey(objeto)) {
				qtdChamadasAtual = chamadasMap.get(nomeMetodo).get(objeto);
			}
		}
		Assert.assertEquals(qtdChamadasEsperada, qtdChamadasAtual);
	}

	private void registrarChamada(String nomeMetodo, Object objeto) {
		if (!chamadasMap.containsKey(nomeMetodo)) {
			chamadasMap.put(nomeMetodo, new HashMap<>());
		}
		Map<Object, Integer> chamadasMapMetodoAtual = chamadasMap.get(nomeMetodo);
		if (!chamadasMapMetodoAtual.containsKey(objeto)) {
			chamadasMapMetodoAtual.put(objeto, 1);
		} else {
			chamadasMapMetodoAtual.put(objeto, chamadasMapMetodoAtual.get(objeto) + 1);
		}
	}

}
