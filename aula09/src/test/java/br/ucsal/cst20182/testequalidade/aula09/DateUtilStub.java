package br.ucsal.cst20182.testequalidade.aula09;

public class DateUtilStub extends DateUtil {

	private Integer anoAtual;

	@Override
	public Integer obterAnoAtual() {
		return anoAtual;
	}

	public void definirAnoAtual(Integer anoAtual) {
		this.anoAtual = anoAtual;
	}

}
