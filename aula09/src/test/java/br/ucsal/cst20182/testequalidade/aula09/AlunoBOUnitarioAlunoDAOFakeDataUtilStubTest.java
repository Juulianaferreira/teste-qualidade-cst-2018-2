package br.ucsal.cst20182.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlunoBOUnitarioAlunoDAOFakeDataUtilStubTest {

	private DateUtil dateUtilStub;

	private AlunoDAO alunoDAO;

	private AlunoBO alunoBO;

	@Before
	public void setup() {
		dateUtilStub = new DateUtilStub();
		alunoDAO = new AlunoDAOFake();
		alunoBO = new AlunoBO(alunoDAO, dateUtilStub);
	}

	@Test
	public void testarCalculoIdade() {

		// Configurando o ano atual no DateUtilStub
		((DateUtilStub) dateUtilStub).definirAnoAtual(2017);

		// Definindo os dados de entrada
		Integer matricula = 5;
		Aluno alunoAtivo = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(1998).build();
		alunoBO.atualizar(alunoAtivo);

		// Definindo a sa�da esperada
		Integer idadeEsperada = 19;

		// Executando o m�todo que est� sendo testado e obtendo o resultado
		// atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Comparando o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

}
