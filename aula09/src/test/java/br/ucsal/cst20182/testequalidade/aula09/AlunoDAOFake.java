package br.ucsal.cst20182.testequalidade.aula09;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAOFake extends AlunoDAO {

	private List<Aluno> alunos = new ArrayList<>();

	@Override
	public void salvar(Aluno aluno) {
		alunos.add(aluno);
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		return null;
	}
}
