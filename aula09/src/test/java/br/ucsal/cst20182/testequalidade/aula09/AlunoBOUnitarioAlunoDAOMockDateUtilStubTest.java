package br.ucsal.cst20182.testequalidade.aula09;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlunoBOUnitarioAlunoDAOMockDateUtilStubTest {

	private DateUtil dateUtilStub;

	private AlunoDAO alunoDAOMock;

	private AlunoBO alunoBO;

	@Before
	public void setup() {
		dateUtilStub = new DateUtilStub();
		alunoDAOMock = new AlunoDAOMock();
		alunoBO = new AlunoBO(alunoDAOMock, dateUtilStub);
	}

	@Test
	public void testarCalculoIdade() {

		// Configurando o ano atual no DateUtilStub
		((DateUtilStub) dateUtilStub).definirAnoAtual(2017);

		// Definindo os dados de entrada
		Integer matricula = 5;

		// Definindo a sa�da esperada
		Integer idadeEsperada = 19;

		// Executando o m�todo que est� sendo testado e obtendo o resultado
		// atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Comparando o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);

		// Verificar se o m�todo encontrarPorMatricula da inst�ncia alunoDAO foi chamado,
		// passando como par�metro a matr�cula 5.
		((AlunoDAOMock)alunoDAOMock).verificarChamada("encontrarPorMatricula", 5, 1);

	}

	@Test
	public void testarAtualizacaoAlunoAtivo() {
		// Definir os dados de entrada
		Aluno alunoAtivo = AlunoBuilder.umAlunoAtivo().build();

		// Definir a sa�da esperada
		// O m�todo atualizar(Aluno aluno) tem retorno void, ent�o o "resultado
		// esperado" � que o alunoDAO.salvar(aluno) seja chamado se o aluno
		// estiver ATIVO e que N�O seja chamado se o aluno estiver CANCELADO.

		// Executar o m�todo que est� sendo testado
		alunoBO.atualizar(alunoAtivo);

		// Verificar se o m�todo salvar da inst�ncia alunoDAO foi chamado,
		// passando como par�metro o objeto alunoAtivo.
		((AlunoDAOMock)alunoDAOMock).verificarChamada("salvar", alunoAtivo, 1);
	}

	@Test
	public void testarAtualizacaoAlunoCancelado() {
		// Definir os dados de entrada
		Aluno alunoCancelado  = AlunoBuilder.umAlunoCancelado().build();

		// Definir a sa�da esperada
		// O m�todo atualizar(Aluno aluno) tem retorno void, ent�o o "resultado
		// esperado" � que o alunoDAO.salvar(aluno) seja chamado se o aluno
		// estiver ATIVO e que N�O seja chamado se o aluno estiver CANCELADO.

		// Executar o m�todo que est� sendo testado
		alunoBO.atualizar(alunoCancelado);

		// Verificar se o m�todo salvar da inst�ncia alunoDAO foi chamado,
		// passando como par�metro o objeto alunoAtivo.
		((AlunoDAOMock)alunoDAOMock).verificarChamada("salvar", alunoCancelado, 0);
	}

}
