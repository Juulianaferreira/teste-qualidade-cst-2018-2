package br.ucsal.cst20182.testequalidade.aula09;

public class AlunoBO {

	private AlunoDAO alunoDAO;

	private DateUtil dateUtil;

	public AlunoBO(AlunoDAO alunoDAO, DateUtil dateUtil) {
		this.alunoDAO = alunoDAO;
		this.dateUtil = dateUtil;
	}

	public void atualizar(Aluno aluno) {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			alunoDAO.salvar(aluno);
		}
	}

	public Integer calcularIdade(Integer matricula) {
		Aluno aluno = alunoDAO.encontrarPorMatricula(matricula);
		return dateUtil.obterAnoAtual() - aluno.getAnoNascimento();
	}

}
