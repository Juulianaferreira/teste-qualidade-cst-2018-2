package br.ucsal.cst20182.testequalidade.aula09.business;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.cst20182.testequalidade.aula09.data.TestUsers;
import br.ucsal.cst20182.testequalidade.aula09.data.UserBuilder;
import br.ucsal.cst20182.testequalidade.aula09.domain.User;

public class LoginBO2Test {

	@Test
	public void validar1UsuarioAlunoComTestDataBuilder() {
		User user1 = UserBuilder.aUser().doTipoAluno().semPassword().build();
		Boolean validacaoEsperada = true;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

	@Test
	public void validar2UsuarioAlunoComTestDataBuilder() {
		UserBuilder userBuilder = UserBuilder.aUser().comName("Jaca").doTipoAluno().semPassword();
		User user1 = userBuilder.build();
		User user2 = userBuilder.mas().comName("caju").build();
		User user3 = userBuilder.build();
		Boolean validacaoEsperada = true;
		Boolean validacaoAtual = LoginBO.validarLogin(user1);
		Assert.assertEquals(validacaoEsperada, validacaoAtual);
	}

}
