package br.ucsal.cst20182.testequalidade.aula09.data;

import br.ucsal.cst20182.testequalidade.aula09.domain.User;

// Sugest�o 2: Aplicar o Object Mother no Test Data Builder.
// Essa � a que mais me agrada.
// Pessoalmente: n�o uso Object Mother separado. Fa�o Test Data Builder que possui diversos Builders customizados.
public class UserBuilderMother {
	public static final String DEFAULT_NAME = "John Smith";
	public static final String DEFAULT_ROLE = "ALUNO";
	public static final String DEFAULT_PASSWORD = "42";

	private String username;
	private String password = DEFAULT_PASSWORD;
	private String role = DEFAULT_ROLE;
	private String name = DEFAULT_NAME;

	private UserBuilderMother() {
	}

	public static UserBuilderMother aUser() {
		return new UserBuilderMother();
	}

	public static UserBuilder umUsuarioProfessor() {
		return UserBuilder.aUser().comName("Claudio Neiva").comUsername("antoniocp").doTipoProfessor();
	}

	public static UserBuilder umUsuarioAluno() {
		return UserBuilder.aUser().comName("Silvio").comUsername("antoniocp").comPassword("gelado").doTipoAluno();

	}

	public UserBuilderMother comName(String name) {
		this.name = name;
		return this;
	}

	public UserBuilderMother comUsername(String username) {
		this.username = username;
		return this;
	}

	public UserBuilderMother comPassword(String password) {
		this.password = password;
		return this;
	}

	public UserBuilderMother semPassword() {
		this.password = null;
		return this;
	}

	public UserBuilderMother doTipoProfessor() {
		this.role = "PROFESSOR";
		return this;
	}

	public UserBuilderMother doTipoAluno() {
		this.role = "ALUNO";
		return this;
	}

	public UserBuilderMother doTipo(String role) {
		this.role = role;
		return this;
	}

	public UserBuilderMother mas() {
		return UserBuilderMother.aUser().doTipo(role).comName(name).comPassword(password).comUsername(username);
	}

	public User build() {
		return new User(name, username, password, role);
	}
}
