package br.ucsal.cst20182.testequalidade.aula09.data;

import br.ucsal.cst20182.testequalidade.aula09.domain.User;

// Padr�o Object Mother
public class TestUsers {

	public static User umUsuarioProfessor() {
		// return new User("Antonio Claudio", "antoniocp", "maria",
		// "PROFESSOR");
		User user1 = new User();
		user1.setName("Claudio Neiva");
		user1.setUserName("antoniocp");
		user1.setRole("PROFESSOR");
		user1.setPassword("maria");
		return user1;
	}

	public static User umUsuarioAluno() {
		User user1 = new User();
		user1.setName("Silvio");
		user1.setUserName("svn");
		user1.setRole("ALUNO");
		user1.setPassword("gelado");
		return user1;
	}
}
