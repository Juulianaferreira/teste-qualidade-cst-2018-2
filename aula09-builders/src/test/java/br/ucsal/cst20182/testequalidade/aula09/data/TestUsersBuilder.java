package br.ucsal.cst20182.testequalidade.aula09.data;

// Sugest�o 1: Aplicar o Test Data Builder no Object Mother.
public class TestUsersBuilder {

	public static UserBuilder umUsuarioProfessor() {
		return UserBuilder.aUser().comName("Claudio Neiva").comUsername("antoniocp").doTipoProfessor();
	}

	public static UserBuilder umUsuarioAluno() {
		return UserBuilder.aUser().comName("Silvio").comUsername("antoniocp").comPassword("gelado").doTipoAluno();
	}
}
